#!/bin/bash

BOLD="$( echo -e "\e[1m" )"
RED="$( echo -e "\e[31m" )"
GREEN="$( echo -e "\e[32m" )"
YELLOW="$( echo -e "\e[33m" )"
NORMAL="$( echo -e "\e[0m" )"

function die {
  echo -e $*
  exit 1
}

# Perform some basic sanity checks on a lab code directory
# $1: Directory of project
function check_lab_dir {
  local DIR=$1

  [ -d "${DIR}" ] || die "${RED}${DIR}${NORMAL} is not a directory"
  [ -f "${DIR}/.cproject" ] || die "${RED}${DIR}${NORMAL} is not a SimplicityStudio project"

  # Fatal problems.
  grep --quiet 'file:/opt' ${DIR}/.cproject \
    && die "${RED}${DIR}${NORMAL} contains hardcoded UNIX paths and will not work in the lab. Please import the project in the lab, select the proper SDK in the project settings and then run this script again."

  grep --quiet 'StudioSdkPathFromID:com.silabs.sdk.exx32' ${DIR}/.cproject \
    && die "${RED}${DIR}${NORMAL} uses the UNIX sdk. Please import the project in the lab, select the proper SDK in the project settings and then run this script again."

  # Mild warnings. If this script becomes out of date, these may not be fatal.
  # Update them if the lab machines ever get newer versions of the software!
  grep --quiet 'arm.toolchain.gnu.cdt:7.2.1' ${DIR}/.cproject || echo -e "${YELLOW}${DIR}${NORMAL} Appears to use a different toolchain from the lab. Please make sure it imports and builds properly."
}

# Pack up a lab to distribute it.
# The generated zip file can be imported as an "archive" in
# Simplicity Studio and does not need to be manually unpacked.
# $1: Directory of project
# $2: Zip file to generate
function pack_lab_dir {
  local DIR="$1"
  local ZIP="$2"

  # Not enough arguments
  [ -n "${ZIP}" ] || die "No zip file name provided for ${RED}${DIR}${NORMAL}"

  # Don't clobber existing files, that's risky
  [ -f "${ZIP}" ] && echo -e "${YELLOW}${ZIP}${NORMAL} already exists. Not overwriting." && return 1

  local ZIPDIR=$(dirname "${ZIP}")
  [ -d "${ZIPDIR}" ] || die "${RED}${ZIPDIR}${NORMAL} is not a directory. Can't create archive."

  check_lab_dir "$1" || return 1

  echo -e "${GREEN}${DIR}${NORMAL} -> ${ZIP}"

  # Remove auto-generated build artifacts that are of no use to the students
  rm -r "${DIR}/GNU ARM"* 2>/dev/null

  zip -r "${ZIP}" "${DIR}"

}

DATE=$(date +%y%m%d)

mkdir -p dist
pack_lab_dir dat278_Lab1 "dist/Lab1_${DATE}.zip"
pack_lab_dir dat278_Lab2 "dist/Lab2_${DATE}.zip"
pack_lab_dir dat278_Lab3 "dist/Lab3_${DATE}.zip"
pack_lab_dir dat278_Lab3_solution "dist/Lab3_solution_${DATE}.zip"
