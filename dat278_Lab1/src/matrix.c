#include <stdint.h>
#include <stdbool.h>
#include <em_core.h>
#include <em_device.h>
#include <em_chip.h>
#include <em_cmu.h>
#include <em_emu.h>
#include <bsp.h>
#include <segmentlcd.h>
#include <bsp_trace.h>
#include <string.h>
#include <stdio.h>

volatile uint32_t sysTicks; /* counts 1ms timeTicks since boot*/


void MemoryCopy(void* dest, void* src, size_t bytes);
void MatrixMultiply(unsigned n, int* c, int* a, int* b);
void MatrixSort(unsigned n, int* a);

/**************************************************************************//**
 * @brief SysTick_Handler
 *   Interrupt Service Routine for system tick counter
 * @note
 *   No wrap around protection
 *****************************************************************************/
void SysTick_Handler(void)
{
  CORE_DECLARE_IRQ_STATE;
  CORE_ENTER_ATOMIC();
  sysTicks += 1;
  CORE_EXIT_ATOMIC();
}


/***************************************************************************
 * A utility function to copy a long sequence of data in memory
 * This implementation is quite naive, feel free to try to improve on it!
 **************************************************************************/
void MemoryCopySimple(void* dest, void* src, size_t bytes) {
  unsigned char* dest_b = (unsigned char*) dest;
  unsigned char* src_b = (unsigned char*) src;

  for (size_t n=0; n<bytes; ++n) {
    dest_b[n] = src_b[n];
  }
}


/**************************************************************************//**
 * @brief Our main workload: multiplication of square matrices
 * @param n the size of all the supplied matrices
 * @param a an nxn matrix of integers
 * @param b an nxn matrix of integers
 * @param c allocated space for the product of a and b (nxn integers)
 *****************************************************************************/
#define MAT(mat,n,i,j) mat[((i)*(n))+(j)]
#define N 32
void MatrixMultiplyLinear(unsigned n, int* c, int* a, int* b) {

  // Temporary working space
  // This is necessary since either a or b might be the same matrix as c.
  int alpha[n*n];
  int beta[n*n];
  int gamma[n*n];

  // Copy the two input matrices
  MemoryCopy(alpha, a, sizeof(alpha));
  MemoryCopy(beta, b, sizeof(beta));

  for(unsigned int i=0;i<n;++i) {
    for(unsigned int j=0;j<n;++j) {
      MAT(gamma,n,i,j) = 0;
      for(unsigned int k=0; k<n; ++k) {
        MAT(gamma,n,i,j) = MAT(gamma,n,i,j) + (MAT(alpha,n,i,k) * MAT(beta,n,k,j));
      }
    }
  }

  // Write out the result
  MemoryCopy(c, gamma, n*n*sizeof(int));
}

// Sort an nxn array in-place
void MatrixSortBubble(unsigned n, int* a) {
  int l = n*n;
  bool swapped = true;

  while (swapped) {
    swapped = false;
    for (int i=1; i<l; ++i) {
      if (a[i-1] > a[i]) {
        // Swap
        int x = a[i-1];
        a[i-1] = a[i];
        a[i] = x;
        swapped = true;
      }
    }
    l = l - 1;
  }

}


//
// Here's an easy place to replace the implementation
// of any of these functions.
//

void MemoryCopy(void* dest, void* src, size_t bytes) {
  MemoryCopySimple(dest, src, bytes);
}

void MatrixMultiply(unsigned n, int* c, int* a, int* b) {
  MatrixMultiplyLinear(n, c, a, b);
}

void MatrixSort(unsigned n, int* a) {
  MatrixSortBubble(n, a);
}



void ShowSpeed(int count) {
  char buf[20];
  snprintf(buf, 20, "%d ms", count);
  SegmentLCD_Write(buf);


}

bool TestFunction() {


  {
    // Test the MemoryCopy() function with something as large as our matrices
    int src[N*N];
    int dst[N*N];
    for (int i=0; i<(N*N); i++) src[i] = (-i);  // Lots of set bits
    for (int i=0; i<(N*N); i++) dst[i] = 0;
    MemoryCopy(dst, src, sizeof(dst));
    for (int i=0; i<(N*N); i++) if (src[i] != dst[i]) return false;
  }


  {
    // A small test of MatrixMultiply, to make sure it works
    int A[3*3] = {  1, -2, 5,
                   -2,  0, 3,
                    7, -4, 8 };

    int B[3*3] = {  2,  9, -12,
                    0,  3, -1,
                    4, -9,  5 };


    int answer[3*3] = {
        22, -42,  15,
        8,  -45,  39,
        46, -21, -40
    };


    int C[3*3];
    MatrixMultiply(3, C, A, B);

    for (int i=0; i<3*3; i++) {
      if (C[i] != answer[i]) return false;
    }
	}


  {
    // A small test of MatrixSort, to make sure THAT works
    int D[4*4]      = { 4, 8, 0, -3, 25, 12, 2, 0, 10 ,-10, 5, 0, 2, 15, 15, -1 };
    int answer[4*4] = { -10, -3, -1, 0, 0, 0, 2, 2, 4, 5, 8, 10, 12, 15, 15, 25 };
    MatrixSort(4, D);
    for (int i=0; i<16; i++) {
      if (D[i] != answer[i]) return false;
    }
	}


  return true;

}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
void SetupData(int n, int* A, int* B) {
  // Generate some data more interesting than zeroes and such
  for (int i=0; i<n; i++) {
    for (int j=0; j<n; j++) {
      int sgn_a = i % 2 ? -1 : 1;
      int sgn_b = j % 3 ? -1 : 1;

      MAT(A,n,i,j) = sgn_a*i*j - (N/(j+1));
      MAT(B,n,i,j) = sgn_b*i*j + (N/(i+1));
    }
  }
}

void die(const char* message) {
  // Enable LCD so we can show that there's no experiment running
  SegmentLCD_Init(false);
  SegmentLCD_Write(message);

  // Halt and catch fire
  while (1) {};
}


int main(void)
{

  sysTicks = 0;

  // Chip errata
  CHIP_Init();

  BSP_LedsInit();
  BSP_LedSet(0);
  BSP_LedSet(1);

  // If first word of user data page is non-zero, enable eA Profiler trace
  // This allows us to correlate energy use with the code we execute.
  BSP_TraceProfilerSetup();

  if (!TestFunction()) {
    // At least one of the unit tests failed.
    // That means the program isn't going to do what it's supposed to do.
    // No point in continuing.
    die("FAILURE");
  }

  // Set the core clock divider, reducing the clock frequency
  CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_8);

  // Setup SysTick Timer for 1 msec interrupts
  if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000)) die("ERROR");

  //SegmentLCD_Write("START");
  BSP_LedClear(0);

  // Some matrices to multiply
  int A[N*N];
  int B[N*N];
  int C[N*N];

  SetupData(N, A, B);


  // Continuously perform matrix multiplications
  uint32_t start_time = sysTicks;
  for (int n_iterations=0; n_iterations<10; n_iterations++) {

    // What we're really computing here is C = A * B * B * B * B
    MatrixMultiply(N,C,A,B);    // C = A * B
    MatrixMultiply(N,C,C,B);    // C = C * B
    MatrixMultiply(N,C,C,B);    // C = C * B
    MatrixMultiply(N,C,C,B);    // C = C * B


    // Generate "new" data to work on next iteration
    MAT(A,N,0,0) += MAT(C,N,0,0);
    MAT(B,N,N-1,N-1) -= MAT(C,N,N-1,N-1);

  }

  // Finally, Sort the values, for some reason
  MatrixSort(N, C);

  BSP_LedClear(1);

  // Show the time it took on the display.
  // Useful for double-checking, perhaps
  if (false) {

    // Enable LCD without voltage boost
    SegmentLCD_Init(false);

    uint32_t duration = sysTicks - start_time;
    ShowSpeed(duration);
    while (true) {
      EMU_EnterEM2(true); // EM4 shuts down the display, so that's not helpful.
    }
  }


  // Execution complete. Go to sleep.
  EMU_EnterEM4();
}

