#include <stdint.h>
#include <stdbool.h>
#include <em_core.h>
#include <em_device.h>
#include <em_chip.h>
#include <em_cmu.h>
#include <em_emu.h>
#include <bsp.h>
#include <segmentlcd.h>
#include <rtcdriver.h>
#include <bsp_trace.h>
#include <string.h>
#include <stdio.h>

#include "dma.h"

volatile unsigned sysTicks; /* counts 1ms timeTicks since boot*/



void MemoryCopy(void* dest, void* src, size_t bytes);
void MatrixMultiply(unsigned int n, int* c, int* a, int* b);
void Delay(unsigned int ms);

/**************************************************************************//**
 * @brief SysTick_Handler
 *   Interrupt Service Routine for system tick counter
 * @note
 *   No wrap around protection
 *****************************************************************************/
void SysTick_Handler(void)
{
  CORE_DECLARE_IRQ_STATE;
  CORE_ENTER_ATOMIC();
  sysTicks += 1;
  CORE_EXIT_ATOMIC();
}

// A rather inefficient way to wait,
// this is known as "Busy waiting", because the processor
// is actually executing instructions the entire time while waiting.
void DelayBusy(unsigned int ms) {
  uint32_t target = sysTicks + ms;
  while (sysTicks < target) {}
}

// A more efficient sleep
void DelaySleep(unsigned int ms) {

  RTCDRV_TimerID_t rtc_sleep_timer;
  RTCDRV_AllocateTimer(&rtc_sleep_timer);

  // Schedule an interrupt that will wake us
  RTCDRV_StartTimer(rtc_sleep_timer, rtcdrvTimerTypeOneshot, ms, NULL, NULL);

  // Deep sleep until then
  // Since other interrupts may also wake the system,
  // we must potentially go back to sleep until the time
  // has actually passed.
  // This may look like busy-waiting, but it's far better.
  bool running = true;
  while (true) {
    RTCDRV_IsRunning(rtc_sleep_timer, &running);
    if (running) {
      CORE_DECLARE_IRQ_STATE;
      CORE_ENTER_ATOMIC();
      EMU_EnterEM2(true);
      CORE_EXIT_ATOMIC();
    } else {
      break;
    }
  }

  RTCDRV_FreeTimer(rtc_sleep_timer);
}

/***************************************************************************
 * A utility function to copy a long sequence of data in memory
 * This implementation is quite naive, feel free to try to improve on it!
 **************************************************************************/
void MemoryCopySimple(void* dest, void* src, size_t bytes) {
  unsigned char* dest_b = (unsigned char*) dest;
  unsigned char* src_b = (unsigned char*) src;

  for (size_t n=0; n<bytes; ++n) {
    dest_b[n] = src_b[n];
  }
}


/**************************************************************************//**
 * @brief Our main workload: multiplication of square matrices
 * @param n the size of all the supplied matrices
 * @param a an nxn matrix of integers
 * @param b an nxn matrix of integers
 * @param c allocated space for the product of a and b (nxn integers)
 *****************************************************************************/
#define MAT(mat,n,i,j) mat[((i)*(n))+(j)]
#define N 32
void MatrixMultiplyLinear(unsigned n, int* c, int* a, int* b) {

  // Temporary working space
  // This is necessary since either a or b might be the same matrix as c.
  int alpha[n*n];
  int beta[n*n];
  int gamma[n*n];

  // Copy the two input matrices
  MemoryCopy(alpha, a, sizeof(alpha));
  MemoryCopy(beta, b, sizeof(beta));

  for(unsigned int i=0;i<n;++i) {
    for(unsigned int j=0;j<n;++j) {
      MAT(gamma,n,i,j) = 0;
      for(unsigned int k=0; k<n; ++k) {
        MAT(gamma,n,i,j) = MAT(gamma,n,i,j) + (MAT(alpha,n,i,k) * MAT(beta,n,k,j));
      }
    }
  }

  // Write out the result
  MemoryCopy(c, gamma, n*n*sizeof(int));
}


//
// Here's an easy place to replace the implementation
// of any of these functions.
//

void MemoryCopy(void* dest, void* src, size_t bytes) {
  MemoryCopySimple(dest, src, bytes);

  //uint32_t channel = MemoryCopyDMA(dest, src, bytes);
  //WaitForDMA(channel);
}

void MatrixMultiply(unsigned n, int* c, int* a, int* b) {
  MatrixMultiplyLinear(n, c, a, b);
}

void Delay(unsigned ms) {
  DelayBusy(ms);
  //DelaySleep(ms);
}


bool TestFunction() {


  {
    // Test the MemoryCopy() function with something as large as our matrices
    int src[N*N];
    int dst[N*N];
    for (int i=0; i<(N*N); i++) src[i] = (-i);  // Lots of set bits
    for (int i=0; i<(N*N); i++) dst[i] = 0;
    MemoryCopy(dst, src, sizeof(dst));
    for (int i=0; i<(N*N); i++) if (src[i] != dst[i]) return false;
  }


  // A small test of MatrixMultiply, to make sure it works
  {
    int A[3*3] = {  1, -2, 5,
                   -2,  0, 3,
                    7, -4, 8 };

    int B[3*3] = {  2,  9, -12,
                    0,  3, -1,
                    4, -9,  5 };


    int answer[3*3] = {
        22, -42,  15,
        8,  -45,  39,
        46, -21, -40
    };


    int C[3*3];
    MatrixMultiply(3, C, A, B);

    for (int i=0; i<3*3; i++) {
      if (C[i] != answer[i]) return false;
    }
	}

  return true;

}

/**************************************************************************//**
 * @brief  Main function
 *****************************************************************************/
void SetupData(int n, int* A, int* B, int* C, int* D) {
  // Generate some data more interesting than zeroes and such
  for (int i=0; i<n; i++) {
    for (int j=0; j<n; j++) {
      int sgn_a = i % 2 ? -1 : 1;
      int sgn_b = j % 3 ? -1 : 1;
      int sgn_c = j % 5 ? -1 : 1;
      int sgn_d = j % 7 ? -1 : 1;

      MAT(A,n,i,j) = sgn_a*i*j - (N/(j+1));
      MAT(B,n,i,j) = sgn_b*i*j + (N/(i+1));
      MAT(C,n,i,j) = sgn_c*i*j*j - (N/(j+1));
      MAT(D,n,i,j) = sgn_d*i*i*j + (N/(i+1));
    }
  }
}


void StartUselessPeripherals() {
  // Turn on all on-board oscillators
  CMU_OscillatorEnable(cmuOsc_AUXHFRCO, true, true);
  CMU_OscillatorEnable(cmuOsc_HFXO, true, true);
  CMU_OscillatorEnable(cmuOsc_LFRCO, true, true);
  CMU_OscillatorEnable(cmuOsc_LFXO, true, true);

  // Switch the processor to use the powerhungriest, superfastest clock
  CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);

  // Enable clocks to all core and peripheral modules
  // This makes them consume energy
  // (the way to turn them off is to cut their clock signals)
  CMU->HFCORECLKEN0 = 0xFFFFFFFF;
  CMU->HFPERCLKEN0  = 0xFFFFFFFF;
}

void die(const char* message) {
  // Enable LCD so we can show that there's no experiment running
  SegmentLCD_Init(false);
  SegmentLCD_Write(message);

  // Halt and catch fire
  while (1) {};
}

int main(void)
{

  sysTicks = 0;

  // Chip errata
  CHIP_Init();

  // Set up the Real-Time Clock hardware
  // This can help us generate a single scheduled interrupt
  // after some set delay.
  RTCDRV_Init();

  // Let's see what difference it makes if we just turn on some stuff we don't actually need
  //StartUselessPeripherals();

  BSP_LedsInit();
  BSP_LedSet(0);
  BSP_LedSet(1);

  // If first word of user data page is non-zero, enable eA Profiler trace
  // This allows us to correlate energy use with the code we execute.
  BSP_TraceProfilerSetup();


  SetupDMA();

  if (!TestFunction()) {
    // At least one of the unit tests failed.
    // That means the program isn't going to do what it's supposed to do.
    // No point in continuing.
    die("FAILURE");
  }

  // Set the core clock divider, reducing the clock frequency
  // Check the documentation, any power of two up to 512 is supported.
  CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_4);


  // Setup SysTick Timer for 1 msec interrupts
  // This takes into account the actual frequency of the system clock,
  // so even if you change the frequency divider it should still tick at the correct rate.
  if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000)) die("ERROR");


  BSP_LedClear(0);
  BSP_LedSet(1);

  // Some matrices to multiply
  int A[N*N];
  int B[N*N];
  int C[N*N];
  int D[N*N];
  int S[N*N];
  int T[N*N];

  SetupData(N, A, B, C, D);


  // Continuously perform matrix multiplications
  for (int n_iterations=0; n_iterations<10; n_iterations++) {
    uint32_t iter_start_time = sysTicks;

    // What we're really computing here is C = A * B * C * D
    MatrixMultiply(N,S,A,B);    // S = A * B
    MatrixMultiply(N,T,C,D);    // T = C * D
    MatrixMultiply(N,S,S,T);    // S = A * B * C * D


    // Generate "new" data to work on next iteration
    MAT(A,N,0,0)     += MAT(C,N,0,0);
    MAT(B,N,N-1,N-1) -= MAT(C,N,N-1,N-1);
    MAT(C,N,0,0)     += MAT(C,N,1,1);
    MAT(D,N,N-1,N-1) -= MAT(C,N,N-2,N-2);

    // Ensure that each iteration lasts exactly two seconds
    int32_t sleep_time = 2000 - (sysTicks - iter_start_time);
    if (sleep_time > 0) {
      Delay(sleep_time);
    } else {
      // The work already took longer than allowed. System is too slow.
      die("TOOSLOW");
    }
  }

  BSP_LedClear(0);
  BSP_LedClear(1);

  // Execution complete. Go to sleep.
  EMU_EnterEM4();
}

