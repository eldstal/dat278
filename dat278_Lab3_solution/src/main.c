#include <stdint.h>
#include <stdbool.h>
#include <em_core.h>
#include <em_device.h>
#include <em_chip.h>
#include <em_cmu.h>
#include <em_emu.h>
#include <em_msc.h>
#include <em_acmp.h>
#include <bsp.h>
#include <segmentlcd.h>
#include <rtcdriver.h>
#include <bsp_trace.h>
#include <string.h>
#include <stdio.h>

#include "globals.h"
#include "common_lesense.h"
#include "slider.h"
#include "lightsensor.h"
#include "temperature.h"
#include "pwmled.h"
#include "lcsense.h"
#include "rtcdriver.h"
#include "game.h"


/*
 * Original measures 16.6mA
 * The following optimizations have been applied:
 *
 * + Reduced clock frequency (div8)(and HFRCO oscillator)
 * + PWM handled entirely in hardware
 * + Button inputs interrupt driven
 * +    With these two, we don't need IDLE() anymore!
 * + Reduce SysTick interval to 10ms
 * + Replace IDLE()  with a 10ms sleep
 * These together yield an idle current of 2.73mA
 *
 * More optimizations:
 * + Core frequency div = 16
 * + Replace DelayMilli with EM1 sleep (copy/paste from lab2): 2.20mA
 * + Remove the LCSense() calls and turn off LED1 (not in spec) : 1.73mA
 *

 * + Enable scan complete interrupt in LESense rather than polling/waiting
 * + Move gecko icon and slider checks inside the LESense interrupt handler
 *
 * + Make game logic reactive (update on button press and use RTC interrupt for delays)
 * + Use an RTC timer to delay temperature measurements.
 * +   With these two, the main loop can drop to EM1.
 * + Disable SysTick entirely to keep it from waking system up
 * + Reduce LESENSE scan frequency to 4Hz
 * Idle current: 1.67mA
 *
 * + Set GCC optimization to -O3: 1.66mA
 */


static void setupCMU(void)
{
  SystemCoreClockUpdate();

  // Gotta go fast
  //CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO);
  CMU_ClockDivSet(cmuClock_CORE, cmuClkDiv_16);

  CMU_OscillatorEnable(cmuOsc_AUXHFRCO, true, true);
  CMU_OscillatorEnable(cmuOsc_HFXO, true, true);
  CMU_OscillatorEnable(cmuOsc_LFRCO, true, true);
  CMU_OscillatorEnable(cmuOsc_LFXO, true, true);

  CMU_ClockSelectSet(cmuClock_LFA, cmuSelect_LFRCO);
  CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_Disabled);

  CMU_ClockEnable(cmuClock_HFPER, true);
  CMU_ClockEnable(cmuClock_GPIO, true);
  CMU_ClockEnable(cmuClock_ACMP0, true);
  CMU_ClockEnable(cmuClock_ACMP1, true);
  CMU_ClockEnable(cmuClock_PRS, true);
  CMU_ClockEnable(cmuClock_CORELE, true);
  CMU_ClockEnable(cmuClock_LESENSE, true);
  CMU_ClockEnable(cmuClock_DAC0, true);

  CMU_ClockDivSet(cmuClock_LESENSE, cmuClkDiv_1);

  SystemCoreClockUpdate();

}




void die(const char* message) {
  SegmentLCD_Write(message);
  // Halt and catch fire
  while (1) {};
}

int main(void)
{


  // Chip errata
  CHIP_Init();


  // Code correlation for the profiler
  BSP_TraceProfilerSetup();

  setupCMU();
  SetupGlobals();
  SetupCommonLESense();
  SetupLightSensor();
  SetupTemperature();
  SetupPWM();
  SetupSlider();
  SetupGame();



  // Caches add latency, we can't have that. Turn it off!
  MSC_EnableCache(false);

  RTCDRV_TimerID_t temp_timer;
  RTCDRV_AllocateTimer(&temp_timer);


  SegmentLCD_Init(false);

  int iteration=0;
  while (true) {

    //IDLE();

    // This is done by interrupt now.
    //ReadCommonLESense();

    // Update the display. This can't be done from an interrupt handler safely.
    Game();

    // Light-sensitive lizard
    // Moved to an interrupt handler.
    //SegmentLCD_Symbol(LCD_SYMBOL_GECKO, !LightSensor());

    // The spec says nothing about LCSense or LED1, so we can just remove them. Nice.
    /*
    if (LCSense()) {
      BSP_LedSet(1);
    } else {
      BSP_LedClear(1);
    }
    */


    bool timer_running = true;
    RTCDRV_IsRunning(temp_timer, &timer_running);

    // Every 400ms, check the temperature
    if (!timer_running) {
      RTCDRV_StartTimer(temp_timer, rtcdrvTimerTypeOneshot, 400, NULL, NULL);

      float temp = Temperature();
      SegmentLCD_Number((int) (temp * 100.0));
      SegmentLCD_Symbol(LCD_SYMBOL_DP10, 1);   // Decimal point
      SegmentLCD_Symbol(LCD_SYMBOL_DEGC, 1);  // Celcius
    }

    IDLE();

    // Moved to an interrupt handler
    /*
    if (Slider() >= 0) {
      PWMIntensity(Slider());
      //SegmentLCD_Number((int) (Slider() * 100));
    }
    */


    IDLE();

    //Interrupts will wake us up when sensor values change, etc.
    EMU_EnterEM1();

    iteration++;
  }

  DelayMilli(5000);

  // Execution complete. Go to sleep.
  EMU_EnterEM4();
}

