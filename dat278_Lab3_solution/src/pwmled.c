#include <bsp.h>
#include <em_timer.h>
#include <em_letimer.h>
#include <em_cmu.h>
#include <em_prs.h>

#include "globals.h"

/*
 * Implementation stolen from
 * https://www.silabs.com/community/blog.entry.html/2015/07/17/chapter_5_part_4---WneD
 */

#define LED_PORT  gpioPortE
#define LED_PIN   2

#define TIMER_TOP                   100
#define DUTY_CYCLE                  1
#define TIMER_CHANNEL               2



void PWMLed() {
  /* Not needed anymore
   * Don't call it from IDLE() either!
   */

}


void PWMIntensity(float intensity) {
  if (intensity < 0) intensity = 0.0;
  if (intensity > 1) intensity = 1.0;

  uint16_t duty_cycle = (uint16_t) (intensity * 100);

  // Set the PWM duty cycle here!
  TIMER_CompareBufSet(TIMER3, TIMER_CHANNEL, duty_cycle);
}


void SetupPWM() {


  // Enable LED output
  GPIO_PinModeSet(LED_PORT, LED_PIN, gpioModePushPull, 0);

  // This uses a regular timer, which will not remain enabled during EM2 sleep.
  // Still a good stability improvement, though!

  CMU_ClockEnable(cmuClock_TIMER3, true);


  // Create the timer count control object initializer
  TIMER_InitCC_TypeDef timerCCInit = TIMER_INITCC_DEFAULT;
  timerCCInit.mode = timerCCModePWM;
  timerCCInit.cmoa = timerOutputActionToggle;

  // Configure CC channel 2
  TIMER_InitCC(TIMER3, TIMER_CHANNEL, &timerCCInit);

  // Route CC2 to location 1 (PE3) and enable pin for cc2
  TIMER3->ROUTE |= (TIMER_ROUTE_CC2PEN | TIMER_ROUTE_LOCATION_LOC1);

  // Set Top Value
  TIMER_TopSet(TIMER3, TIMER_TOP);


  // Create a timerInit object, based on the API default
  TIMER_Init_TypeDef timerInit = TIMER_INIT_DEFAULT;
  timerInit.prescale = timerPrescale256;

  TIMER_Init(TIMER3, &timerInit);


  PWMIntensity(0.3f);
}

