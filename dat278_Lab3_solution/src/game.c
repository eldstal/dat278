#include <em_gpio.h>
#include <em_core.h>
#include <em_rtc.h>
#include <string.h>
#include <segmentlcd.h>
#include <stdio.h>

#include <gpiointerrupt.h>
#include <rtcdriver.h>

#include "game.h"
#include "globals.h"


/*
 * Code taken from the gpiointerrupt example project
 * It requires the emdrv/gpiointerrupt includes/source code (see Drivers/gpiointerrupt.c)
 */

#define BTN_PORT gpioPortB
#define BTN_0_PIN 9
#define BTN_1_PIN 10

bool BUTTON_DOWN[2];
bool BUTTON_UP[2];

char GAMESCREEN[10];

void gameUpdateState();

static struct {
  char player;
  int player_pos;
  int target_pos;
  int score;
  int reward;
  int oumpf;
  char* message;

  enum {
    MESSAGE,
    SCORE,
    PLAY,
    EGG
  } mode;

  RTCDRV_TimerID_t timer;

} GameState;

void Buttons() {
  /*
   * Not needed anymore.
   * Don't call this from IDLE().
   */
}

static void gpioCallback(uint8_t pin)
{
  CORE_DECLARE_IRQ_STATE;
  CORE_ENTER_ATOMIC();

  if (pin == BTN_0_PIN) {
    BUTTON_DOWN[0] = true;
    BUTTON_UP[0] = true;
  } else if (pin == BTN_1_PIN) {
    BUTTON_DOWN[1] = true;
    BUTTON_UP[1] = true;
  }

  // Update game state, now that we've received some input
  gameUpdateState();

  CORE_EXIT_ATOMIC();
}

static void setupInterrupts() {
  /* Initialize GPIO interrupt dispatcher */
  GPIOINT_Init();

  /* Configure PB9 and PB10 as input */
  GPIO_PinModeSet(BTN_PORT, BTN_0_PIN, gpioModeInput, 0);
  GPIO_PinModeSet(BTN_PORT, BTN_1_PIN, gpioModeInput, 0);

  /* Register callbacks before setting up and enabling pin interrupt. */
  GPIOINT_CallbackRegister(BTN_0_PIN, gpioCallback);
  GPIOINT_CallbackRegister(BTN_1_PIN, gpioCallback);

  /* Set falling edge interrupt for both ports */
  GPIO_IntConfig(BTN_PORT, BTN_0_PIN, false, true, true);
  GPIO_IntConfig(BTN_PORT, BTN_1_PIN, false, true, true);
}

void moveTarget() {
  while (GameState.player_pos == GameState.target_pos) GameState.target_pos = RTC_CounterGet() % 7;
}



// If a button has been pressed, reset it and return 1 or 2.
// Return 0 if no buttons have been pressed
int check_button() {
  for (int b=0; b<2; b++) {
    if (BUTTON_UP[b]) {
      BUTTON_DOWN[b] = false;
      BUTTON_UP[b] = false;
      return b+1;
    }
  }
  return 0;
}

static void timerElapsed(RTCDRV_TimerID_t id, void *user) {
  gameUpdateState();
}

static void setCountdown(unsigned int ms) {
  bool timer_running = true;
  RTCDRV_IsRunning(GameState.timer, &timer_running);

  if (!timer_running) {
    RTCDRV_StartTimer(GameState.timer, rtcdrvTimerTypeOneshot, ms, timerElapsed, NULL);
  }

  gameUpdateState();
}


void nextGameState() {
  bool timer_running = true;
  RTCDRV_IsRunning(GameState.timer, &timer_running);

  if (timer_running) return;
  if (GameState.mode == PLAY) return;

  switch (GameState.mode) {
  case MESSAGE:
    GameState.mode = SCORE;
    setCountdown(1000);
    break;

  case EGG:
    break;

  case SCORE:
  default:
    GameState.mode = PLAY;
    setCountdown(1000);
    break;
  }
}

void gameUpdateState() {


  // The string shown on the main part of the LCD
  strncpy(GAMESCREEN,"         ", 10);

  switch (GameState.mode) {
  case SCORE:
    snprintf(GAMESCREEN, 10, "%d Pts", GameState.score);
    break;

  case MESSAGE:
    snprintf(GAMESCREEN, 10, "%s", GameState.message);
    break;

  case PLAY:{
    int btn = check_button();
    if (btn == 1) GameState.player_pos--;
    if (btn == 2) GameState.player_pos++;
    if (GameState.player_pos < 0) { GameState.player_pos = 0; GameState.oumpf++; }
    if (GameState.player_pos > 6) GameState.player_pos = 6;
    GAMESCREEN[GameState.target_pos] = 'X';
    GAMESCREEN[GameState.player_pos] = GameState.player;

    /* IDLE(); */

    if (GameState.oumpf >= 10) {
      GameState.mode = EGG;
      setCountdown(1000);
    }

    if (GameState.player_pos == GameState.target_pos) {
      GameState.score += GameState.reward;

      moveTarget();

      GameState.message = "BRILLANT";
      GameState.mode = MESSAGE;
      setCountdown(1000);
    }

  }
    break;

  case EGG:
    GameState.oumpf = 0;
    GameState.message = "HACKER";
    GameState.mode = MESSAGE;
    GameState.score += 1336;
    GameState.player = 'O';
    setCountdown(1000);
    break;
  default:
    GameState.mode = PLAY;
  }

  nextGameState();
}

void Game() {
  /* IDLE(); */
  SegmentLCD_Write(GAMESCREEN);
}


void SetupGame() {
  BUTTON_DOWN[0] = false;
  BUTTON_DOWN[1] = false;
  BUTTON_UP[0] = false;
  BUTTON_UP[1] = false;

  GameState.player = 'o';
  GameState.player_pos = 2;
  GameState.score = 0;
  GameState.reward = 1;
  GameState.oumpf = 0;
  GameState.message = "WELCOME";
  GameState.mode = MESSAGE;
  RTCDRV_AllocateTimer(&GameState.timer);
  moveTarget();

  setupInterrupts();

  setCountdown(1000);

}
