/*
 * globals.h
 *
 *  Created on: 30 nov. 2019
 *      Author: Albin
 */

#ifndef SRC_GLOBALS_H_
#define SRC_GLOBALS_H_

void SetupGlobals();


// Easy way to just get rid of the calls, if needed
// As it turns out, it's a decent optimization to just have IDLE() sleep for a few ms.
// This helps a lot if the rest of the program is full-speed busy-waiting!
//#define IDLE() __IDLE()
#define IDLE() {}

void __IDLE();

void DelayTicks(unsigned int ticks);
void DelayMilli(unsigned int ms);

// An incrementing counter of 0.1ms ticks.
extern volatile unsigned int sysTicks;

#endif /* SRC_GLOBALS_H_ */
