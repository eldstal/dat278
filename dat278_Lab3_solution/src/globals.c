/*
 * globals.c
 *
 *  Created on: 30 nov. 2019
 *      Author: Albin
 */

#include <stddef.h>
#include <em_core.h>
#include <em_cmu.h>
#include <em_emu.h>
#include <rtcdriver.h>


#include "globals.h"
#include "pwmled.h"
#include "game.h"

volatile unsigned int sysTicks;



// This isn't necessary anymore. Renamed so that we can easily eliminate all calls to it
void __IDLE() {
  // We don't need to poll buttons or anything in here now.
  // In some places, IDLE is used to wait for something, so
  // we may as well sleep for 10ms or so
  DelayMilli(10);
}


void DelayTicks(unsigned int t) {
  uint32_t target = sysTicks + t;
  while (sysTicks < target) { }
}

// A more efficient sleep
void DelaySleep(unsigned int ms) {

  RTCDRV_TimerID_t rtc_sleep_timer;
  RTCDRV_AllocateTimer(&rtc_sleep_timer);

  // Schedule an interrupt that will wake us
  RTCDRV_StartTimer(rtc_sleep_timer, rtcdrvTimerTypeOneshot, ms, NULL, NULL);

  // Light sleep until then
  bool running = true;
  while (true) {
    RTCDRV_IsRunning(rtc_sleep_timer, &running);
    if (running) {
      CORE_DECLARE_IRQ_STATE;
      CORE_ENTER_ATOMIC();
      EMU_EnterEM1();
      CORE_EXIT_ATOMIC();
    } else {
      break;
    }
  }

  RTCDRV_FreeTimer(rtc_sleep_timer);
}

void DelayMilli(unsigned int ms) {
  //DelayTicks(ms / 10);
  DelaySleep(ms);
}


void SysTick_Handler(void)
{
  CORE_DECLARE_IRQ_STATE;
  CORE_ENTER_ATOMIC();
  sysTicks += 1;
  CORE_EXIT_ATOMIC();
}


void SetupGlobals() {

  sysTicks = 0;

  RTCDRV_Init();

  // Call SysTick_Handler at 10ms intervals
  // Since we've turned everything to RTC and timers and interrupts, we don't need the systick at all!
  //if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 100)) while (1) {};
}
