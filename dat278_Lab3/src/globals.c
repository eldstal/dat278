/*
 * globals.c
 *
 *  Created on: 30 nov. 2019
 *      Author: Albin
 */

#include <em_core.h>
#include <em_cmu.h>

#include "pwmled.h"
#include "game.h"

volatile unsigned int sysTicks;



// Global function.
// Call this every now and then, because some things have to
// run very often.
void IDLE() {
  // HACK: If we take too long before flipping the LEDs, the intensity illusion
  // is broken. As long as IDLE() gets called often enough, it should be fine.
  // It's fine. Don't worry about it.
  PWMLed();

  // Don't miss button presses just because it took long to poll them again
  Buttons();
}


void DelayTicks(unsigned int t) {
  uint32_t target = sysTicks + t;
  while (sysTicks < target) { IDLE(); }
}

void DelayMilli(unsigned int ms) {
  DelayTicks(ms * 10);
}


void SysTick_Handler(void)
{
  CORE_DECLARE_IRQ_STATE;
  CORE_ENTER_ATOMIC();
  sysTicks += 1;
  CORE_EXIT_ATOMIC();
}


void SetupGlobals() {

  sysTicks = 0;

  // Call SysTick_Handler at 0.1ms intervals
  if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 10000)) while (1) {};
}
